# Deep Learning Models

## Pre-requisites:

* Python version > 3.8
* PyTorch
* Cuda
* Jupyter Notebook

## How to check results and documentation of my implementation:

Open results.html in a web browser for the corresponding sub-project.

## How to run:

To run projects navigate to respective directories in Jupyter notebook and open the ipynb files.